﻿using Solstice.Data.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Solstice.Service.services
{
    public interface IContactService
    {
        List<Contact> GetContacts();
        Contact GetContact(int id);
    }
}