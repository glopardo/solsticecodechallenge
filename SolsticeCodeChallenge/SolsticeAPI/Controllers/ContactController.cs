﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Configuration;
using Solstice.Service.services;
using Solstice.Data.models;

namespace SolsticeAPI.Controllers
{
    public class ContactController : ApiController
    {
        private ContactService contactService = new ContactService();
        public List<Contact> Get()
        {
            return contactService.GetContacts();
        }
    }
}
