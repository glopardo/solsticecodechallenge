﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Solstice.Data.models
{
    public class Phone
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public string Number { get; set; }
    }
}