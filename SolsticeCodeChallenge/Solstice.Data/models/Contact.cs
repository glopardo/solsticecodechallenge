﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Solstice.Data.models
{
    public class Contact
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public string ProfilePictureUrl { get; set; }
        public string Email { get; set; }
        public DateTime BirthDate { get; set; }
        public List<Phone> Phone { get; set; }
        public string Address { get; set; }
    }
}