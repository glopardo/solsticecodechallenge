﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Solstice.Data.models
{
    public class SolsticeDB : DbContext
    {
        public SolsticeDB() : base("Data Source=solstice.cjxaog3xcczr.us-east-2.rds.amazonaws.com,1433;Initial Catalog=codeChallenge;User ID=sa;Password=solstice1234")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
        public DbSet<Phone> Phones { get; set; }
        public DbSet<Contact> Contacts { get; set; }
    }
}